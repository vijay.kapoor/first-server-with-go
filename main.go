package main

//Importa os pacotes necessarios
import (
	"log"
	"net/http"
	"time"
)

//Faca a funcao main que sera sua funcao principal
//que rodara seu programa go
func main() {
	//Executa a funcao para subir o servidor
	//e coloca o defer em frente para ocorrer
	//depois do log de sucesso
	defer StartServer()

	//Loga sucesso na conexao
	log.Print("Conexao feita com sucesso")
}

//Faca uma funcao publica para cuidar do retorno da conexao
func handler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hello World"))
}

//Faca uma funcao privada para subir o servidor
func StartServer() {
	//Declara e inicializa a variavel duration
	duration, _ := time.ParseDuration("1000ns")

	//Declara e inicializa o Server e inclui '&' antes
	//para declarar um ponteiro
	server := &http.Server{
		Addr:              "192.168.0.12:5000",
		IdleTimeout:       duration,
	}

	//Usa a funcao para tratar as rotas e seus retornos
	http.HandleFunc("/", handler)

	//Loga somente se ocorrer falha na conexao
	log.Fatal(server.ListenAndServe())
}
